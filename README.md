# README #

This README would normally document whatever steps are necessary to get your cluster up and running.

1.Cluster installation needs to be done based on documentation

2.Authorization needs to be enabed via active directory

3.Storage and persistent volume needs to be configured based on requirement

4.Network policy needs to be enabled to segregate projects and its users

5.Need to configure internal & external monitoring based on project requirement

6.Need to enable alert manager to notify support regarding critical alerts via mail receiver

7.Elastic search,Fluentd & Kibana needs to be configured to enable logging

8.Centralized image management needs to be configured to maintain images - Base imagae management

9.Need to enable continuous deployment based on project approach