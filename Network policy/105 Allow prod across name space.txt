apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-n2-B-to-n1-A
  namespace: N1 --<Project Owner of N1, to create and apply this policy>
spec:
  podSelector:
    matchLabels:
      deployment-A-pod-label: deployment-A-pod-label-value --<Label to match pod A in namespace N1>
  policyTypes:
  - Ingress
ingress:
  - from:
    -  namespaceSelector:
        matchLabels:
          networking/namespace: N2--<Label to match the namespace N2>
       podSelector:
        matchLabels:
          deployment-B-pod-label-key: deployment-B-pod-label-value --<Label to match pod B in namespace N2>